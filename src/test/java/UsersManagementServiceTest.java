
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.edu.pw.reversi.um.entity.Users;
import pl.edu.pw.reversi.um.repository.UsersRepository;
import pl.edu.pw.reversi.um.service.UsersManagementService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * This class contains unit tests for the UsersManagementService.
 * It tests various scenarios including positive cases, negative cases,
 * and exception handling.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
class UsersManagementServiceTest {
    @Mock
    private UsersRepository usersRepository;

    private UsersManagementService usersManagementService;

    /**
     * Initializes mocks and the UsersManagementService object before each test.
     */
    @BeforeEach
    public void init() {
        try (AutoCloseable mockito = MockitoAnnotations.openMocks(this)) {
            usersManagementService = new UsersManagementService(usersRepository);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests the retrieval of all users.
     */
    @Test
    void getAllUsersTest() {
        List<Users> usersList = new ArrayList<>();
        Users user = new Users();
        user.setEmail("test@test.com");
        usersList.add(user);

        when(usersRepository.findAllByOrderByTotalPointsDesc()).thenReturn(usersList);

        List<Users> returnedUsers = usersManagementService.getAllUsers();
        assertEquals(usersList, returnedUsers);
    }

    /**
     * Tests the registration of a new user.
     */
    @Test
    void registerUserTest() {
        Users user = new Users();
        user.setEmail("test@test.com");

        when(usersRepository.save(any(Users.class))).thenReturn(user);

        assertTrue(usersManagementService.registerUser(user));
    }

    /**
     * Tests the authentication of a user.
     */
    @Test
    void authenticateUserTest() {
        Users user = new Users();
        user.setEmail("test@test.com");
        user.setHashedPassword("hashedPassword");

        when(usersRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        assertTrue(usersManagementService.authenticateUser(user));
    }

    /**
     * Tests the unregistration of a user.
     */
    @Test
    void unregisterUserTest() {
        doNothing().when(usersRepository).deleteById(anyLong());

        assertTrue(usersManagementService.unregisterUser(1L));
    }

    /**
     * Tests updating a user's information.
     */
    @Test
    void updateUserTest() {
        Users user = new Users();
        user.setUserId(1L);
        user.setEmail("test@test.com");

        when(usersRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(usersRepository.save(any(Users.class))).thenReturn(user);

        assertTrue(usersManagementService.updateUser(user));
    }

    /**
     * Tests the retrieval of a user by email.
     */
    @Test
    void getUserByEmailTest() {
        Users user = new Users();
        user.setEmail("test@test.com");

        when(usersRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        Users returnedUser = usersManagementService.getUserByEmail("test@test.com");
        assertEquals(user, returnedUser);
    }

    /**
     * Tests the retrieval of a user by ID.
     */
    @Test
    void getUserByIdTest() {
        Users user = new Users();
        user.setEmail("test@test.com");

        when(usersRepository.findById(anyLong())).thenReturn(Optional.of(user));

        Users returnedUser = usersManagementService.getUserById(1L);
        assertEquals(user, returnedUser);
    }

    /**
     * Tests registration of a 'null' user.
     */
    @Test
    void registerUserTest_whenUserNull() {

        assertFalse(usersManagementService.registerUser(null));
    }

    /**
     * Tests authentication of non-existing user.
     */
    @Test
    void authenticateUserTest_whenUserDoesNotExist() {
        Users user = new Users();
        user.setEmail("non-existing@test.com");
        user.setHashedPassword("hashedPassword");

        when(usersRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertFalse(usersManagementService.authenticateUser(user));
    }

    /**
     * Tests authentication with incorrect password.
     */
    @Test
    void authenticateUserTest_whenPasswordIncorrect() {
        Users wrongUser = new Users();
        wrongUser.setEmail("test@test.com");
        wrongUser.setHashedPassword("wrongPassword");

        Users user = new Users();
        user.setEmail("test@test.com");
        user.setHashedPassword("hashedPassword");

        when(usersRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        assertFalse(usersManagementService.authenticateUser(wrongUser));
    }

    /**
     * Tests user unregistration when exception occurs.
     */
    @Test
    void unregisterUserTest_whenExceptionOccurs() {
        doThrow(new RuntimeException()).when(usersRepository).deleteById(anyLong());

        assertFalse(usersManagementService.unregisterUser(1L));
    }

    /**
     * Tests update of non-existing user.
     */
    @Test
    void updateUserTest_whenUserDoesNotExist() {
        Users user = new Users();
        user.setUserId(1L);
        user.setEmail("test@test.com");

        when(usersRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertFalse(usersManagementService.updateUser(user));
    }

    /**
     * Tests user query byEmail for non-existing user.
     */
    @Test
    void getUserByEmailTest_whenUserDoesNotExist() {
        when(usersRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertNull(usersManagementService.getUserByEmail("non-existing@test.com"));
    }

    /**
     * Tests user query byId for non-existing user.
     */
    @Test
    void getUserByIdTest_whenUserDoesNotExist() {
        when(usersRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertNull(usersManagementService.getUserById(1L));
    }

    /**
     * Tests logout of authenticated user.
     */
    @Test
    void logoutUser_whenUserIsAuthenticated() {
        Users user = new Users();
        user.setUserId(1L);
        user.setEmail("test@test.com");
        user.setHashedPassword("hashedPassword");

        when(usersRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        assertTrue(usersManagementService.authenticateUser(user));

        assertTrue(usersManagementService.logoutUser(1L));
    }

    /**
     * Tests logout of non-authenticated user.
     */
    @Test
    void logoutUser_whenUserIsNotAuthenticated() {

        assertFalse(usersManagementService.logoutUser(1L));
    }
}

/**
 * This class contains exceptional test cases for the UsersManagementService.
 * It tests scenarios where exceptions are expected to be handled by the service.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@ExtendWith(MockitoExtension.class)
class UserManagementServiceExceptionalTest {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private List<Users> authenticatedUsers;

    @InjectMocks
    private UsersManagementService usersManagementService;

    private Users user;

    /**
     * Sets up the user object and mocks before each test.
     */
    @BeforeEach
    public void setUp() {

        user = new Users();
        user.setUserId(1L);
        user.setName("John");
        user.setHashedPassword("hashedPassword");
    }

    /**
     * Tests if the save method of UsersRepository is called when saving a user.
     */
    @Test
    void saveUser_ShouldCallSaveMethodOfUsersRepository() {
        usersManagementService.saveUser(user);

        verify(usersRepository, times(1)).save(user);
    }

    /**
     * Tests handling exceptions when saving a user fails.
     */
    @Test
    void saveUser_ShouldCatchExceptionWhenSaveFails() {
        doThrow(new RuntimeException()).when(usersRepository).save(user);

        usersManagementService.saveUser(user);

        verify(usersRepository, times(1)).save(user);
    }

    /**
     * Tests handling exceptions during user authentication.
     */
    @Test
    void authenticateUser_ShouldCatchException() {
        when(usersRepository.findByEmail(anyString())).thenThrow(new RuntimeException());

        Boolean result = usersManagementService.authenticateUser(user);

        assertFalse(result);
    }

    /**
     * Tests handling exceptions during user list retrieval.
     */
    @Test
    void getAllUser_ShouldCatchException() {
        when(usersRepository.findAllByOrderByTotalPointsDesc()).thenThrow(new RuntimeException());

        List<Users> result = usersManagementService.getAllUsers();

        assertTrue(result.isEmpty());
    }

    /**
     * Tests handling exceptions during user query byEmail.
     */
    @Test
    void getUserByEmail_ShouldCatchException() {
        when(usersRepository.findByEmail(anyString())).thenThrow(new RuntimeException());

        Users result = usersManagementService.getUserByEmail(anyString());

        assertNull(result);
    }

    /**
     * Tests handling exceptions during user query byId.
     */
    @Test
    void findUserById_ShouldCatchException() {
        when(usersRepository.findById(anyLong())).thenThrow(new RuntimeException());

        Users result = usersManagementService.getUserById(anyLong());

        assertNull(result);
    }

    /**
     * Tests handling exceptions during user registration.
     */
    @Test
    void registerUser_ShouldCatchExceptionWhenSaveFails() {
        doThrow(new RuntimeException()).when(usersRepository).save(user);

        Boolean result = usersManagementService.registerUser(user);

        assertFalse(result);
    }

    /**
     * Tests handling exceptions during user unregistration.
     */
    @Test
    void unregisterUser_ShouldCatchExceptionWhenDeleteFails() {

        doThrow(new RuntimeException()).when(usersRepository).deleteById(anyLong());

        Boolean result = usersManagementService.unregisterUser(anyLong());

        assertFalse(result);
    }

    /**
     * Tests update of existing user using not-null password.
     */
    @Test
    void updateUser_UserExists_HashedPasswordNotNull_UpdateSuccessful() {
        when(usersRepository.findById(1L)).thenReturn(Optional.of(new Users()));

        Boolean result = usersManagementService.updateUser(user);

        assertTrue(result);
    }

    /**
     * Tests update of existing user using null password.
     */
    @Test
    void updateUser_UserExists_HashedPasswordNull_UpdateSuccessful() {
        user.setHashedPassword(null);
        when(usersRepository.findById(1L)).thenReturn(Optional.of(new Users()));

        Boolean result = usersManagementService.updateUser(user);

        assertTrue(result);
    }

    /**
     * Tests update of non-existing user.
     */
    @Test
    void updateUser_UserDoesNotExist_ReturnFalse() {
        when(usersRepository.findById(1L)).thenReturn(Optional.empty());

        Boolean result = usersManagementService.updateUser(user);

        assertFalse(result);
    }

    /**
     * Tests handling exceptions during user update.
     */
    @Test
    void updateUser_CatchException_ReturnFalse() {
        when(usersRepository.findById(1L)).thenThrow(RuntimeException.class);

        Boolean result = usersManagementService.updateUser(user);

        assertFalse(result);
    }

    /**
     * Tests handling exceptions during user logout.
     */
    @Test
    void logoutUser_CatchException_ReturnFalse() throws NoSuchFieldException, IllegalAccessException {
        when(authenticatedUsers.stream()).thenThrow(RuntimeException.class);

        Field field = UsersManagementService.class.getDeclaredField("authenticatedUsers");
        field.setAccessible(true);
        field.set(usersManagementService, authenticatedUsers);

        Boolean result = usersManagementService.logoutUser(1L);

        assertFalse(result);
    }
}
