import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import pl.edu.pw.reversi.UserManagementApplication;
import pl.edu.pw.reversi.gs.controller.GameSessionController;
import pl.edu.pw.reversi.gs.dto.*;
import pl.edu.pw.reversi.gs.model.GameSessionStatus;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Integration tests for the GameSessionController.
 * This class tests the complete behavior of the controller by interacting with a live application context.
 * It requires running game engine sever on local machine, port 8090.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@SpringBootTest(classes = UserManagementApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GameSessionControllerIntegrationTest {

    private final GameSessionController gameSessionController;

    /**
     * Constructs the integration test class and autowires the GameSessionController.
     *
     * @param gameSessionController the GameSessionController to be used in tests.
     */
    @Autowired
    public GameSessionControllerIntegrationTest(GameSessionController gameSessionController) {
        this.gameSessionController = gameSessionController;
    }

    /**
     * Tests the creation of a game session.
     *
     * @param playerId the ID of the player.
     * @param tempId temporary ID used for testing.
     */
    @ParameterizedTest
    @Order(1)
    @CsvSource({"1, 1", "1, null", "2, 2", "2, null", "5, null"})
    void testCreateGame(Long playerId, String tempId) {
        Long gameId = "null".equals(tempId) ? null : Long.valueOf(tempId);
        ResponseEntity<CreateGameResponse> response = gameSessionController.create(playerId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(gameId, response.getBody().getGameId());
    }

    /**
     * Tests retrieving the status of a game session with status "NEW".
     *
     * @param gameId the ID of the game.
     * @param tempStatus temporary status used for testing.
     */
    @ParameterizedTest
    @Order(2)
    @CsvSource({"1, NEW", "2, NEW"})
    void testGameStatusNew(Long gameId, String tempStatus) {
        GameSessionStatus gameSessionStatus = GameSessionStatus.valueOf(tempStatus);
        ResponseEntity<GameStatusResponse> response = gameSessionController.status(gameId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(gameSessionStatus, response.getBody().getGameSessionStatus());
    }

    /**
     * Tests the functionality of joining a game.
     *
     * @param playerId the ID of the player joining the game.
     * @param gameId the ID of the game to be joined.
     * @param tempId temporary ID used for testing.
     */
    @ParameterizedTest
    @Order(3)
    @CsvSource({"1, 2, null", "2, 1, null", "3, 1, 1", "4, 1, null", "5, 1, null", "6, 2, null"})
    void testJoinGame(Long playerId, Long gameId, String tempId) {
        Long joinId = "null".equals(tempId) ? null : Long.valueOf(tempId);
        JoinGameRequest request = new JoinGameRequest();
        request.setPlayerId(playerId);

        ResponseEntity<JoinGameResponse> response = gameSessionController.join(gameId, request);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(joinId, response.getBody().getGameId());
    }

    /**
     * Tests the retrieval of game list when one game is available.
     */
    @Test
    @Order(4)
    void testGameListWhenOneAvailable() {
        ResponseEntity<GameListResponse> response = gameSessionController.list();
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getGameSessions());
        assertEquals(1, response.getBody().getGameSessions().size());
    }

    /**
     * Tests the scenario of joining a second game as a fourth player.
     */
    @Test
    @Order(5)
    void testJoinSecondGameFourthPlayer() {
        JoinGameRequest request = new JoinGameRequest();
        request.setPlayerId(4L);

        ResponseEntity<JoinGameResponse> response = gameSessionController.join(2L, request);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getGameId());
    }

    /**
     * Tests the retrieval of game list when no games are available.
     */
    @Test
    @Order(6)
    void testGameListWhenZeroAvailable() {
        ResponseEntity<GameListResponse> response = gameSessionController.list();
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getGameSessions());
        assertEquals(0, response.getBody().getGameSessions().size());
    }

    /**
     * Tests retrieving a game by player ID in positive scenarios.
     *
     * @param playerId the ID of the player.
     * @param gameId the ID of the game.
     */
    @ParameterizedTest
    @Order(7)
    @CsvSource({"1, 1", "2, 2", "3, 1", "4, 2"})
    void testGetGameByPlayerIdPositive(Long playerId, Long gameId) {
        ResponseEntity<Long> response = gameSessionController.get(playerId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(gameId, response.getBody().longValue());
    }

    /**
     * Tests retrieving the status of a game session with status "ACTIVE".
     *
     * @param gameId the ID of the game.
     * @param tempStatus temporary status used for testing.
     */
    @ParameterizedTest
    @Order(8)
    @CsvSource({"1, ACTIVE", "2, ACTIVE"})
    void testGameStatusActive(Long gameId, String tempStatus) {
        GameSessionStatus gameSessionStatus = GameSessionStatus.valueOf(tempStatus);
        ResponseEntity<GameStatusResponse> response = gameSessionController.status(gameId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(gameSessionStatus, response.getBody().getGameSessionStatus());
    }

    /**
     * Tests retrieving the status of non-existing game sessions.
     *
     * @param gameId the ID of the game.
     */
    @ParameterizedTest
    @Order(9)
    @CsvSource({"0", "3"})
    void testNotExistingGameStatus(Long gameId) {
        ResponseEntity<GameStatusResponse> response = gameSessionController.status(gameId);
        assertEquals(HttpStatusCode.valueOf(500), response.getStatusCode());
        assertNull(response.getBody());
    }

    /**
     * Tests the retrieval of game board in positive scenarios.
     *
     * @param gameId the ID of the game.
     */
    @ParameterizedTest
    @Order(10)
    @CsvSource({"1", "2"})
    void testGetBoardPositive(Long gameId) {
        ResponseEntity<GetBoardResponse> response = gameSessionController.board(gameId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getBoard().get(3).get(3));
        assertEquals(1, response.getBody().getBoard().get(4).get(4));
        assertEquals(2, response.getBody().getBoard().get(3).get(4));
        assertEquals(2, response.getBody().getBoard().get(4).get(3));
        assertEquals(0, response.getBody().getBoard().get(2).get(2));
    }

    /**
     * Tests the retrieval of game board in negative scenarios.
     *
     * @param gameId the ID of the game.
     */
    @ParameterizedTest
    @Order(11)
    @CsvSource({"3", "4"})
    void testGetBoardNegative(Long gameId) {
        ResponseEntity<GetBoardResponse> response = gameSessionController.board(gameId);
        assertEquals(HttpStatusCode.valueOf(500), response.getStatusCode());
        assertNull(response.getBody());
    }

    /**
     * Tests leaving the game in positive scenarios.
     *
     * @param gameId the ID of the game.
     * @param playerId the ID of the player leaving the game.
     */
    @ParameterizedTest
    @Order(12)
    @CsvSource({"1, 1", "2, 4"})
    void testLeaveGamePositive(Long gameId, Long playerId) {
        ResponseEntity<Void> response = gameSessionController.leave(gameId, playerId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    /**
     * Tests leaving the game in negative scenarios.
     *
     * @param gameId the ID of the game.
     * @param playerId the ID of the player leaving the game.
     */
    @ParameterizedTest
    @Order(13)
    @CsvSource({"1, 2", "2, 3"})
    void testLeaveGameNegative(Long gameId, Long playerId) {
        ResponseEntity<Void> response = gameSessionController.leave(gameId, playerId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    /**
     * Tests leaving the game with null values.
     *
     * @param gameId the ID of the game.
     * @param playerId the ID of the player leaving the game.
     */
    @ParameterizedTest
    @Order(14)
    @CsvSource({"5, 1", "5, 5"})
    void testLeaveGameNull(Long gameId, Long playerId) {
        ResponseEntity<Void> response = gameSessionController.leave(gameId, playerId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
    }

    /**
     * Tests retrieving the status of a game session with status "FINISHED".
     *
     * @param gameId the ID of the game.
     * @param tempStatus temporary status used for testing.
     */
    @ParameterizedTest
    @Order(15)
    @CsvSource({"1, FINISHED", "2, FINISHED"})
    void testGameStatusFinished(Long gameId, String tempStatus) {
        GameSessionStatus gameSessionStatus = GameSessionStatus.valueOf(tempStatus);
        ResponseEntity<GameStatusResponse> response = gameSessionController.status(gameId);
        assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(gameSessionStatus, response.getBody().getGameSessionStatus());
    }
}