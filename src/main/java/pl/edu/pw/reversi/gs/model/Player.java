/**
 * This class represents a player in the game session.
 * It stores information about the player's ID, name, color, score, remaining pawns, and activity status.
 * The player's information can be updated based on the game status.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.reversi.gs.client.GameStatus;

@Setter
@Getter
public class Player {
    Long id;
    String name;
    PawnColors color;
    Integer score;
    Integer remainingPawns;
    Boolean isActive;

    public Player() {
        this.id = 0L;
        this.name = "?";
        this.score = 0;
        this.remainingPawns = 30;
        this.isActive = false;
    }

    /**
     * Updates the player's information based on the provided game status.
     *
     * @param gameStatus The game status containing the updated player information.
     */
    public void updatePlayer(GameStatus gameStatus) {
        if (this.color.getId() == 1) {
            this.score = gameStatus.getPawnsOnBoardPlayer1();
            this.remainingPawns = gameStatus.getPawnsInPocketPlayer1();
        } else {
            this.score = gameStatus.getPawnsOnBoardPlayer2();
            this.remainingPawns = gameStatus.getPawnsInPocketPlayer2();
        }
        this.isActive = this.color.getId() == gameStatus.getWhoHaveTurnNow();
    }
}
