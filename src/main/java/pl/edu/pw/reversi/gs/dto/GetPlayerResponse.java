/**
 * Represents a response containing a player.
 * Contains the player object.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.reversi.gs.model.Player;

@Getter
@Setter
public class GetPlayerResponse {
    private Player player; // The player object
}
