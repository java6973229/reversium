/**
 * Represents a request to join a Reversi game.
 * Contains the player ID of the player joining the game.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinGameRequest {
    private Long playerId; // The player ID of the player joining the game
}
