/**
 * This interface represents a game server for Reversi.
 *
 * @Author Damian Pasik
 * @Version 0.9
 * @Since 11.06.2023
 */
package pl.edu.pw.reversi.gs.client;

public interface IGameServer {
    Long createGame();

    Boolean removeGame(Long gameId);

    Plansza makeMove(Long gameId, Long playerId, Integer x, Integer y);

    Plansza gameInfo(Long gameId);
}
