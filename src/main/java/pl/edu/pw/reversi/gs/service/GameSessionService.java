/**
 * This service class implements the IGameSessionService interface
 * and provides operations for managing game sessions.
 * It interacts with the users management service and the game server.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pw.reversi.gs.client.IGameServer;
import pl.edu.pw.reversi.gs.model.*;
import pl.edu.pw.reversi.um.entity.Users;
import pl.edu.pw.reversi.um.service.IUsersManagementService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class GameSessionService implements IGameSessionService {
    private final IUsersManagementService usersManagementService;
    private final IGameServer gameServer;
    private final List<GameSession> gameSessions = Collections.synchronizedList(new ArrayList<>());

    @Autowired
    public GameSessionService(IUsersManagementService usersManagementService,
                              IGameServer gameServer) {
        this.usersManagementService = usersManagementService;
        this.gameServer = gameServer;
    }

    /**
     * Retrieves a game session by its ID.
     *
     * @param gameId The ID of the game session.
     * @return The game session with the specified ID, or null if not found.
     */
    public GameSession getByGameId(Long gameId) {
        return gameSessions.stream()
                .filter(gameSession -> gameSession.getGameId().equals(gameId))
                .findFirst()
                .orElse(null);
    }

    /**
     * Retrieves a list of game sessions by their status.
     *
     * @param status The status of the game sessions to retrieve.
     * @return A list of game sessions with the specified status.
     */
    public List<GameSession> getByGameStatus(GameSessionStatus status) {
        return gameSessions.stream()
                .filter(gameSession -> status.equals(gameSession.getGameSessionStatus()))
                .toList();
    }

    /**
     * Retrieves the player ID associated with a game session.
     *
     * @param playerId The ID of the player.
     * @return The ID of the game session the player is associated with, or null if not found.
     */
    public Long getByPlayerId(Long playerId) {
        GameSession gameSession = gameSessions.stream()
                .filter(g -> (g.getGameSessionStatus().equals(GameSessionStatus.NEW)
                        || g.getGameSessionStatus().equals(GameSessionStatus.ACTIVE))
                        && g.getPlayers().stream().anyMatch(p -> p.getId().equals(playerId)))
                .findFirst().orElse(null);
        return gameSession != null ? gameSession.getGameId() : null;
    }

    /**
     * Creates a new game session for a player.
     *
     * @param playerId The ID of the player creating the game.
     * @return The ID of the newly created game session.
     */
    public synchronized Long createGame(Long playerId) {
        try {
            GameSession gameSession = getByPlayerId(playerId) == null ?
                    new GameSession(usersManagementService, gameServer) : null;
            if (gameSession != null) {
                gameSession.createGame(playerId);
                gameSessions.add(gameSession);
                return gameSession.getGameId();
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Joins a player to an existing game session.
     *
     * @param gameId   The ID of the game session to join.
     * @param playerId The ID of the player joining the game.
     * @return The ID of the game session the player joined.
     */
    public Long joinGame(Long gameId, Long playerId) {
        try {
            GameSession gameSession = getByGameId(gameId);
            return getByPlayerId(playerId) == null ?
                    gameSession.joinGame(playerId) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Removes a player from a game session.
     *
     * @param gameId   The ID of the game session to leave.
     * @param playerId The ID of the player leaving the game.
     */
    public void leaveGame(Long gameId, Long playerId) {
        try {
            getByGameId(gameId).leaveGame(playerId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the board for a game session.
     *
     * @param gameId The ID of the game session.
     * @return The board associated with the game session.
     */
    public Board getBoard(Long gameId) {
        try {
            GameSession gameSession = getByGameId(gameId);
            return gameSession.getBoard();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves a player from a game session by their index.
     *
     * @param gameId The ID of the game session.
     * @param index  The index of the player.
     * @return The player at the specified index, or null if not found.
     */
    public Player getPlayer(Long gameId, Integer index) {
        try {
            GameSession gameSession = getByGameId(gameId);
            return gameSession.getPlayers().get(index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Waits for a move to be made in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player waiting for the move.
     */
    public void waitForMove(Long gameId, Long playerId) {
        try {
            GameSession gameSession = getByGameId(getByPlayerId(playerId));
            if (gameSession != null) {
                gameSession.waitForTurn();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Waits for a game session to be available for joining.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player waiting for the game session.
     */
    public void waitForGame(Long gameId, Long playerId) {
        try {
            GameSession gameSession = getByGameId(getByPlayerId(playerId));
            if (gameSession != null) {
                gameSession.waitForGame();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Makes a move in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player making the move.
     * @param x        The X coordinate of the move.
     * @param y        The Y coordinate of the move.
     */
    public void makeMove(Long gameId, Long playerId, Integer x, Integer y) {
        try {
            getByGameId(gameId).makeMove(x, y, playerId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes a game session.
     *
     * @param gameSession The game session to be removed.
     */
    public synchronized void removeGame(GameSession gameSession) {
        if (gameSession != null
                && gameSession.getGameSessionStatus().equals(GameSessionStatus.REPORTED)) {
            try {
                gameServer.removeGame(gameSession.getExtGameId());
                gameSessions.remove(gameSession);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Reports the result of a game session.
     *
     * @param gameSession The game session to be reported.
     */
    public void reportGame(GameSession gameSession) {
        if (gameSession != null
                && gameSession.getGameSessionStatus().equals(GameSessionStatus.FINISHED)
                && (gameSession.getPlayers().get(0).getScore()
                + gameSession.getPlayers().get(1).getScore()) > 0) {
            try {
                // tie
                if (gameSession.getPlayers().get(0).getScore()
                        == gameSession.getPlayers().get(1).getScore()) {
                    addPointsToUser(gameSession, 0);
                    addPointsToUser(gameSession, 1);
                } else {

                    if (gameSession.getPlayers().get(0).getScore()
                            > gameSession.getPlayers().get(1).getScore()) {
                        // player 0 won
                        addPointsToUser(gameSession, 0);
                    } else {
                        // player 1 won
                        addPointsToUser(gameSession, 1);
                    }
                }
                gameSession.setGameSessionStatus(GameSessionStatus.REPORTED);
                gameSession.setModificationDate(LocalDateTime.now());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void addPointsToUser(GameSession gs, Integer index) {
        Player player = gs.getPlayers().get(index);
        if (player != null && player.getScore() != 0) {
            Users user = usersManagementService.getUserById(player.getId());
            if (user != null) {
                Long currentPoints = user.getTotalPoints();
                user.setTotalPoints(currentPoints + player.getScore());
                usersManagementService.saveUser(user);
            }
        }
    }

    /**
     * Retrieves the status of a game session.
     *
     * @param gameId The ID of the game session.
     * @return The status of the game session.
     */
    public GameSessionStatus getGameSessionStatus(Long gameId) {
        return getByGameId(gameId).getGameSessionStatus();
    }

    /**
     * Retrieves the result of a game session for a specific player.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return The result of the game session for the specified player.
     */
    public GameResult getGameResult(Long gameId, Long playerId) {
        GameSession gameSession = getByGameId(gameId);
        if (gameSession != null) {
            GameResult gameResult = new GameResult();
            Player player0 = gameSession.getPlayers().get(0);
            Player player1 = gameSession.getPlayers().get(1);
            if (player0.getId().longValue() == playerId.longValue()) {
                gameResult.setIsWinner(player0.getScore() >= player1.getScore());
                gameResult.setScore(player0.getScore());
            }
            if (player1.getId().longValue() == playerId.longValue()) {
                gameResult.setIsWinner(player1.getScore() >= player0.getScore());
                gameResult.setScore(player1.getScore());
            }
            return gameResult;
        }
        return null;
    }
}
