/**
 * Represents the result of a game.
 * Contains information about whether the player is the winner and the player's score.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameResult {

    private Boolean isWinner;
    private Integer score;
}
