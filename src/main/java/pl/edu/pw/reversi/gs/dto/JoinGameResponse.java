/**
 * Represents a response for joining a Reversi game.
 * Contains the game ID of the joined game.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinGameResponse {
    private Long gameId; // The game ID of the joined game
}
