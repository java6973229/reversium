/**
 * Represents the game board for Reversi.
 * The board is represented as a 2D grid of integer values.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.reversi.gs.client.GameStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class Board {
    private List<List<Integer>> board;
    private static final int MX = 8;
    private static final int MY = 8;

    /**
     * Default constructor for the board.
     * Initializes the board with the starting configuration.
     */
    public Board() {
        this.board = new ArrayList<>();
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 3, 0, 0, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 3, 1, 2, 0, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 2, 1, 3, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 0, 3, 0, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0)));
        board.add(new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0)));
    }

    /**
     * Constructor for the board that initializes it based on the game status received from the game server.
     *
     * @param gs the game status received from the game server
     */
    public Board(GameStatus gs) {
        this();
        convert(gs);
    }

    /**
     * Converts the board based on the game status received from the game server.
     * This method updates the state of pawns on the board and highlights possible moves.
     *
     * @param gs the game status received from the game server
     */
    public void convert(GameStatus gs) {
        // handling lack of some data in initial board received from game server
        if (gs.getLastValidRequestReceived() != null) {
            int[][] extBoard = gs.getCurrentBoard();
            for (int x = 0; x < MX; x++) {
                for (int y = 0; y < MY; y++) {
                    setPawn(x, y, extBoard[y][x]);
                }
            }

            int[][] extMoves;
            if (gs.getWhoHaveTurnNow() == 1) {
                extMoves = gs.getPossibleMovesPlayer1();
            } else {
                extMoves = gs.getPossibleMovesPlayer2();
            }

            if (extMoves != null) {
                for (int x = 0; x < MX; x++) {
                    for (int y = 0; y < MY; y++) {
                        if (extMoves[y][x] > 0) {
                            setPawn(x, y, 3);
                        }
                    }
                }
            }
        }
    }

    /**
     * Sets the state of a pawn at the specified position on the board.
     *
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     * @param v the value representing the state of the pawn
     */
    public void setPawn(int x, int y, int v) {
        this.board.get(y).set(x, v);
    }

    /**
     * Gets the state of a pawn at the specified position on the board.
     *
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     * @return the value representing the state of the pawn
     */
    public int getPawn(int x, int y) {
        return this.board.get(y).get(x);
    }
}
