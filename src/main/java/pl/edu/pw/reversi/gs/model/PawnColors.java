/**
 * This enum represents the colors of the pawns in the game.
 * It defines two colors: WHITE and BLACK.
 * Each color is associated with an ID.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

import lombok.Getter;

@Getter
public enum PawnColors {
    WHITE(1L),
    BLACK(2L);

    private final Long id;

    PawnColors(Long id) {
        this.id = id;
    }
}
