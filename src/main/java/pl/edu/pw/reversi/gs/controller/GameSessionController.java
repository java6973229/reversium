/**
 * Controller class that handles game-related API endpoints.
 * Handles operations such as creating a game, joining a game, making moves, and retrieving game information.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.pw.reversi.gs.dto.*;
import pl.edu.pw.reversi.gs.model.Board;
import pl.edu.pw.reversi.gs.model.GameResult;
import pl.edu.pw.reversi.gs.model.GameSession;
import pl.edu.pw.reversi.gs.model.GameSessionStatus;
import pl.edu.pw.reversi.gs.service.IGameSessionService;

import java.util.List;

@RestController
@RequestMapping("/api/game")
public class GameSessionController {

    private final IGameSessionService gameSessionService;

    @Autowired
    public GameSessionController(IGameSessionService gameSessionService) {
        this.gameSessionService = gameSessionService;
    }

    @PostMapping("/create/{playerId}")
    /**
     * Handles the creation of a new game session.
     *
     * @param playerId The ID of the player creating the game
     * @return ResponseEntity<CreateGameResponse> containing the created game ID on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<CreateGameResponse> create(@PathVariable Long playerId) {
        try {
            Long gameId = gameSessionService.createGame(playerId);
            CreateGameResponse response = new CreateGameResponse();
            response.setGameId(gameId);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/join/{gameId}")
    /**
     * Handles the joining of a game session by a player.
     *
     * @param gameId  The ID of the game session to join
     * @param request The JoinGameRequest object containing the player ID
     * @return ResponseEntity<JoinGameResponse> containing the joined game ID on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<JoinGameResponse> join(@PathVariable Long gameId, @RequestBody JoinGameRequest request) {
        try {
            Long sameId = gameSessionService.joinGame(gameId, request.getPlayerId());
            JoinGameResponse response = new JoinGameResponse();
            response.setGameId(sameId);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/leave/{gameId}/{playerId}")
    /**
     * Handles the player leaving a game session.
     *
     * @param gameId   The ID of the game session to leave
     * @param playerId The ID of the player leaving the game session
     * @return ResponseEntity<Void> with an HTTP status code indicating success or failure
     */
    public ResponseEntity<Void> leave(@PathVariable Long gameId, @PathVariable Long playerId) {
        try {
            gameSessionService.leaveGame(gameId, playerId);
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/list")
    /**
     * Retrieves a list of available game sessions.
     *
     * @return ResponseEntity<GameListResponse> containing the list of available game sessions on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<GameListResponse> list() {
        try {
            List<GameSession> availableGameSessions = gameSessionService.getByGameStatus(GameSessionStatus.NEW);
            GameListResponse response = new GameListResponse();
            response.setGameSessions(availableGameSessions);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/game/{playerId}")
    /**
     * Retrieves the ID of the game session associated with a player.
     *
     * @param playerId The ID of the player
     * @return ResponseEntity<Long> containing the game ID associated with the player on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<Long> get(@PathVariable Long playerId) {
        try {
            return ResponseEntity.ok(gameSessionService.getByPlayerId(playerId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/status/{gameId}")
    /**
     * Retrieves the status of a game session.
     *
     * @param gameId The ID of the game session
     * @return ResponseEntity<GameStatusResponse> containing the game session status on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<GameStatusResponse> status(@PathVariable Long gameId) {
        try {
            GameStatusResponse response = new GameStatusResponse();
            response.setGameSessionStatus(gameSessionService.getByGameId(gameId).getGameSessionStatus());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/board/{gameId}")
    /**
     * Retrieves the game board of a game session.
     *
     * @param gameId The ID of the game session
     * @return ResponseEntity<GetBoardResponse> containing the game board on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<GetBoardResponse> board(@PathVariable Long gameId) {
        try {
            Board board = gameSessionService.getBoard(gameId);
            GetBoardResponse response = new GetBoardResponse();
            response.setBoard(board.getBoard());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/player/{gameId}/{index}")
    /**
     * Retrieves the player information of a game session.
     *
     * @param gameId The ID of the game session
     * @param index  The index of the player
     * @return ResponseEntity<GetPlayerResponse> containing the player information on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<GetPlayerResponse> player(@PathVariable Long gameId, @PathVariable Integer index) {
        try {
            GetPlayerResponse response = new GetPlayerResponse();
            response.setPlayer(gameSessionService.getPlayer(gameId, index));
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/wait/{gameId}/{playerId}")
    /**
     * Waits for the player's turn in a game session.
     *
     * @param gameId   The ID of the game session
     * @param playerId The ID of the player
     * @return ResponseEntity<Void> with an HTTP status code indicating success or failure
     */
    public ResponseEntity<Void> wait(@PathVariable Long gameId, @PathVariable Long playerId) {
        try {
            gameSessionService.waitForMove(gameId, playerId);
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/start/{gameId}/{playerId}")
    /**
     * Waits for the game session to start.
     *
     * @param gameId   The ID of the game session
     * @param playerId The ID of the player
     * @return ResponseEntity<Void> with an HTTP status code indicating success or failure
     */
    public ResponseEntity<Void> start(@PathVariable Long gameId, @PathVariable Long playerId) {
        try {
            gameSessionService.waitForGame(gameId, playerId);
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/move/{gameId}/{playerId}")
    /**
     * Handles the player's move in a game session.
     *
     * @param gameId   The ID of the game session
     * @param playerId The ID of the player
     * @param request  The MoveRequest object containing the coordinates of the move
     * @return ResponseEntity<Void> with an HTTP status code indicating success or failure
     */
    public ResponseEntity<Void> move(@PathVariable Long gameId, @PathVariable Long playerId, @RequestBody MoveRequest request) {
        try {
            gameSessionService.makeMove(gameId, playerId, request.getX(), request.getY());
            return ResponseEntity.ok(null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/result/{gameId}/{playerId}")
    /**
     * Retrieves the result of a game session for a specific player.
     *
     * @param gameId   The ID of the game session
     * @param playerId The ID of the player
     * @return ResponseEntity<GameResult> containing the game result on success,
     *         or an error response with an HTTP status code on failure
     */
    public ResponseEntity<GameResult> result(@PathVariable Long gameId, @PathVariable Long playerId) {
        try {
            return ResponseEntity.ok(gameSessionService.getGameResult(gameId, playerId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
