/**
 * Represents a response containing the game board.
 * Contains a 2-dimensional list representing the game board.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class GetBoardResponse {
    private List<List<Integer>> board; // The game board represented as a list of lists
}
