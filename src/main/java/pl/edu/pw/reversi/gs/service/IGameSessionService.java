/**
 * This interface defines the operations for managing game sessions.
 * It provides methods for creating, joining, leaving, and managing game sessions,
 * as well as accessing game-related information.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.service;

import pl.edu.pw.reversi.gs.model.*;

import java.util.List;
public interface IGameSessionService {

    /**
     * Retrieves a game session by its ID.
     *
     * @param id The ID of the game session.
     * @return The game session with the specified ID, or null if not found.
     */
    GameSession getByGameId(Long id);

    /**
     * Retrieves a list of game sessions by their status.
     *
     * @param status The status of the game sessions to retrieve.
     * @return A list of game sessions with the specified status.
     */
    List<GameSession> getByGameStatus(GameSessionStatus status);

    /**
     * Retrieves the player ID associated with a game session.
     *
     * @param playerId The ID of the player.
     * @return The ID of the game session the player is associated with, or null if not found.
     */
    Long getByPlayerId(Long playerId);

    /**
     * Creates a new game session for a player.
     *
     * @param playerId The ID of the player creating the game.
     * @return The ID of the newly created game session.
     */
    Long createGame(Long playerId);

    /**
     * Joins a player to an existing game session.
     *
     * @param gameId   The ID of the game session to join.
     * @param playerId The ID of the player joining the game.
     * @return The ID of the game session the player joined.
     */
    Long joinGame(Long gameId, Long playerId);

    /**
     * Removes a player from a game session.
     *
     * @param gameId   The ID of the game session to leave.
     * @param playerId The ID of the player leaving the game.
     */
    void leaveGame(Long gameId, Long playerId);

    /**
     * Retrieves the board for a game session.
     *
     * @param gameId The ID of the game session.
     * @return The board associated with the game session.
     */
    Board getBoard(Long gameId);

    /**
     * Retrieves a player from a game session by their index.
     *
     * @param gameId The ID of the game session.
     * @param index  The index of the player.
     * @return The player at the specified index, or null if not found.
     */
    Player getPlayer(Long gameId, Integer index);

    /**
     * Makes a move in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player making the move.
     * @param x        The X coordinate of the move.
     * @param y        The Y coordinate of the move.
     */
    void makeMove(Long gameId, Long playerId, Integer x, Integer y);

    /**
     * Waits for a move to be made in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player waiting for the move.
     */
    void waitForMove(Long gameId, Long playerId);

    /**
     * Waits for a game session to be available for joining.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player waiting for the game session.
     */
    void waitForGame(Long gameId, Long playerId);

    /**
     * Removes a game session.
     *
     * @param gameSession The game session to be removed.
     */
    void removeGame(GameSession gameSession);

    /**
     * Reports the result of a game session.
     *
     * @param gameSession The game session to be reported.
     */
    void reportGame(GameSession gameSession);

    /**
     * Retrieves the status of a game session.
     *
     * @param gameId The ID of the game session.
     * @return The status of the game session.
     */
    GameSessionStatus getGameSessionStatus(Long gameId);

    /**
     * Retrieves the result of a game session for a specific player.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return The result of the game session for the specified player.
     */
    GameResult getGameResult(Long gameId, Long playerId);
}
