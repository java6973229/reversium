/**
 * Represents a move request for a Reversi game.
 * Contains the coordinates (x, y) of the desired move.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoveRequest {
    private Integer x; // The x-coordinate of the move
    private Integer y; // The y-coordinate of the move
}
