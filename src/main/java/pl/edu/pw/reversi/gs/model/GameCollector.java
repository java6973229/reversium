/**
 * GameCollector is responsible for managing game sessions and performing scheduled tasks related to game management.
 * It terminates idle games, reports finished games, and removes reported games after a certain delay.
 * The time intervals for these tasks can be configured using properties.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.edu.pw.reversi.gs.service.IGameSessionService;
import pl.edu.pw.reversi.um.service.IUsersManagementService;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
public class GameCollector {

    private final IGameSessionService gameSessionService;
    private final IUsersManagementService usersManagementService;

    @Value("${game.startTimeout:120}")
    private Integer startTimeout;
    @Value("${game.moveTimeout:60}")
    private Integer moveTimeout;

    @Value("${game.purgeDelay:300}")
    private Integer purgeDelay;

    @Autowired
    public GameCollector(IUsersManagementService usersManagementService,
                         IGameSessionService gameSessionService) {
        this.gameSessionService = gameSessionService;
        this.usersManagementService = usersManagementService;
    }

    /**
     * Terminates idle games that have not started within a specified timeout duration.
     * This method is scheduled to run at a fixed rate determined by the property "scheduler.terminateIdleGames".
     */
    @Scheduled(fixedRateString = "${scheduler.terminateIdleGames}")
    public void terminateIdleGames() {
        for(GameSession gs : gameSessionService.getByGameStatus(GameSessionStatus.NEW)) {
            Duration duration = Duration.between(gs.getCreationDate(), LocalDateTime.now());

            if(duration.toSeconds() > startTimeout) {
                gs.terminateGame();
            }
        }

        for(GameSession gs : gameSessionService.getByGameStatus(GameSessionStatus.ACTIVE)) {
            Duration duration = Duration.between(gs.getModificationDate(), LocalDateTime.now());

            if(duration.toSeconds() > moveTimeout) {
                gs.terminateGame();
            }
        }
    }

    /**
     * Reports finished games to the game session service.
     * This method is scheduled to run at a fixed rate determined by the property "scheduler.reportFinishedGames".
     */
    @Scheduled(fixedRateString = "${scheduler.reportFinishedGames}")
    public void reportFinishedGames() {
        for(GameSession gs : gameSessionService.getByGameStatus(GameSessionStatus.FINISHED)) {
            gameSessionService.reportGame(gs);
        }
    }

    /**
     * Removes reported games that have been reported for a specified delay duration.
     * This method is scheduled to run at a fixed rate determined by the property "scheduler.removeReportedGames".
     */
    @Scheduled(fixedRateString = "${scheduler.removeReportedGames}")
    public void removeReportedGames() {
        for(GameSession gs : gameSessionService.getByGameStatus(GameSessionStatus.REPORTED)) {
            Duration duration = Duration.between(gs.getModificationDate(), LocalDateTime.now());

            if(duration.toSeconds() > purgeDelay) {
                gameSessionService.removeGame(gs);
            }
        }
    }
}
