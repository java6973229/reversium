package pl.edu.pw.reversi.gs.client;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LastValidRequestReceived {
    private int player;
    private int row;
    private int col;
}