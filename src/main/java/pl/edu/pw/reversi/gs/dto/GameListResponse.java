/**
 * Represents a response containing a list of game sessions.
 * Contains a list of GameSession objects.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.reversi.gs.model.GameSession;

import java.util.List;

@Getter
@Setter
public class GameListResponse {
    private List<GameSession> gameSessions; // The list of game sessions
}
