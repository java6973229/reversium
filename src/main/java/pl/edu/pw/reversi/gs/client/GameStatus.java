/**
 * This class represents the status of a Reversi game session.
 * It holds information about the game state, such as the current board configuration, available moves,
 * number of pawns on the board and in the players' pockets, the winner, and other relevant data.
 *
 * @Author Damian Pasik
 * @Version 0.9
 * @Since 11.06.2023
 */
package pl.edu.pw.reversi.gs.client;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter

public class GameStatus {
    private Long gameId;
    private int whoHaveTurnNow;
    private int numberOfAvailableMovesPlayer1;
    private int numberOfAvailableMovesPlayer2;
    private int pawnsOnBoardPlayer1;
    private int pawnsOnBoardPlayer2;
    private int pawnsInPocketPlayer1;
    private int pawnsInPocketPlayer2;
    private int theWinner;
    private int freeSpotsOnBoard;
    private boolean gameFinished;
    private String responseForPlayer1;
    private String responseForPlayer2;
    private LastValidRequestReceived lastValidRequestReceived;
    private int[][] currentBoard;
    private ArrayList<String> currentBoardPrintForWeb;
    private int[][] possibleMovesRequestingPlayer;
    private int[][] possibleMovesPlayer1;
    private ArrayList<String> possibleMovesPrint1;
    private int[][] possibleMovesPlayer2;
    private ArrayList<String> possibleMovesPrint2;
}
