/**
 * Game state enumerator
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

public enum GameSessionStatus { NEW, ACTIVE, FINISHED, REPORTED, MISSING }
