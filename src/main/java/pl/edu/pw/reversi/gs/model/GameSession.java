/**
 * Represents a game session.
 * It manages the game state, players, and interactions with the game server.
 * Provides methods to make moves, create/join/leave games, and handle game termination.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.reversi.gs.client.GameStatus;
import pl.edu.pw.reversi.gs.client.IGameServer;
import pl.edu.pw.reversi.gs.client.Plansza;
import pl.edu.pw.reversi.um.service.IUsersManagementService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Setter
@Getter
public class GameSession {
    @JsonIgnore
    private final IUsersManagementService usersManagementService;
    @JsonIgnore
    private final IGameServer gameServer;
    @JsonIgnore
    private static final Random random = new Random();
    @JsonIgnore
    private static final Integer MAX_SCORE = 64;
    @JsonIgnore
    private static long currentGameId = 1L;
    @JsonIgnore
    private CountDownLatch latch;
    private final Long gameId;
    @JsonIgnore
    private final Long extGameId;
    private Board board;
    private List<Player> players;
    private GameSessionStatus gameSessionStatus;
    private LocalDateTime creationDate;
    @JsonIgnore
    private LocalDateTime modificationDate;

    /**
     * Constructs a GameSession instance with the specified dependencies.
     *
     * @param usersManagementService The service for managing users.
     * @param gameServer             The game server client.
     */
    public GameSession(IUsersManagementService usersManagementService, IGameServer gameServer) {
        this.usersManagementService = usersManagementService;
        this.gameServer = gameServer;
        this.gameId = getNextGameId();
        this.players = new ArrayList<>();
        this.gameSessionStatus = GameSessionStatus.NEW;
        this.creationDate = LocalDateTime.now();
        this.modificationDate = LocalDateTime.now();
        this.extGameId = gameServer.createGame();
        this.latch = new CountDownLatch(1);
    }

    /**
     * Generates the next game ID.
     *
     * @return The next game ID.
     */
    private static synchronized long getNextGameId() {
        return currentGameId++;
    }

    /**
     * Makes a move in the game session.
     *
     * @param x        The X coordinate of the move.
     * @param y        The Y coordinate of the move.
     * @param playerId The ID of the player making the move.
     */
    public synchronized void makeMove(int x, int y, Long playerId) {
        if (this.gameSessionStatus.equals(GameSessionStatus.ACTIVE)) {
            try {
                Player player = this.getPlayerById(playerId);
                if (player != null) {
                    Plansza plansza = gameServer.makeMove(extGameId, player.getColor().getId(), x, y);
                    GameStatus gameStatus = plansza.getGameStatus();
                    if (gameStatus != null) {
                        this.board.convert(gameStatus);
                        for (Player testDummy : players) testDummy.updatePlayer(gameStatus);
                        // handling lack of some data in initial board received from game server
                        if (gameStatus.getLastValidRequestReceived() != null) {
                            boolean isGameFinished = gameStatus.isGameFinished();
                            int activePlayer = gameStatus.getWhoHaveTurnNow();
                            boolean isPlayer1Active = (activePlayer == 1);
                            // handling some game termination cases not implemented yet in game server
                            if (isGameFinished || (isPlayer1Active && gameStatus.getNumberOfAvailableMovesPlayer1() == 0) ||
                                    (!isPlayer1Active && gameStatus.getNumberOfAvailableMovesPlayer2() == 0) ||
                                    (isPlayer1Active && gameStatus.getPawnsInPocketPlayer1() == 0) ||
                                    (!isPlayer1Active && gameStatus.getPawnsInPocketPlayer2() == 0)) {
                                this.gameSessionStatus = GameSessionStatus.FINISHED;
                            }
                        }
                        this.modificationDate = LocalDateTime.now();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                latch.countDown();
                this.latch = new CountDownLatch(1);
            }
        }
    }

    /**
     * Creates a new game in the session.
     *
     * @param playerId The ID of the player creating the game.
     */
    public synchronized void createGame(Long playerId) {
        if (this.gameSessionStatus.equals(GameSessionStatus.NEW) && players.isEmpty()) {
            Player player = new Player();
            player.setId(playerId);
            player.setName(usersManagementService.getUserById(playerId).getName());
            this.players.add(player);
            this.players.add(new Player());
            this.setPawnColors();
            Plansza plansza = gameServer.gameInfo(extGameId);
            if (plansza != null) {
                GameStatus gameStatus = plansza.getGameStatus();
                if (gameStatus != null) {
                    this.board = new Board(gameStatus);
                    for (Player testDummy : players) testDummy.updatePlayer(gameStatus);
                    this.modificationDate = LocalDateTime.now();
                }
            }
        }
    }

    /**
     * Joins the game in the session as a player.
     *
     * @param playerId The ID of the player joining the game.
     * @return The ID of the game session the player joined.
     */
    public synchronized Long joinGame(Long playerId) {
        if (this.gameSessionStatus.equals(GameSessionStatus.NEW) && !players.isEmpty()) {
            players.get(1).setId(playerId);
            players.get(1).setName(usersManagementService.getUserById(playerId).getName());
            this.setGameSessionStatus(GameSessionStatus.ACTIVE);
            this.modificationDate = LocalDateTime.now();
            this.latch.countDown();
            this.latch = new CountDownLatch(1);
            return this.getGameId();
        }
        return null;
    }

    /**
     * Leaves the game in the session.
     *
     * @param playerId The ID of the player leaving the game.
     */
    public synchronized void leaveGame(Long playerId) {
        for (Player player : players) {
            if (player.getId().equals(playerId)) {
                player.setScore(0);
                this.terminateGame();
                this.modificationDate = LocalDateTime.now();
            } else {
                player.setScore(MAX_SCORE);
            }
        }
    }

    /**
     * Sets the pawn colors for the players.
     */
    public void setPawnColors() {
        int maxIndex = PawnColors.values().length;
        int rndIndex = random.nextInt(maxIndex);
        for (Player player : players) {
            player.setColor(PawnColors.values()[++rndIndex % 2]);
        }
    }

    /**
     * Retrieves a player in the game session by their ID.
     *
     * @param playerId The ID of the player.
     * @return The player with the specified ID, or null if not found.
     */
    public Player getPlayerById(Long playerId) {
        return players.stream()
                .filter(p -> p.getId().equals(playerId))
                .findFirst()
                .orElse(null);
    }

    /**
     * Waits for the player's turn in the game session.
     */
    public void waitForTurn() {
        try {
            while (this.gameSessionStatus.equals(GameSessionStatus.ACTIVE)) {
                if (latch.await(1, TimeUnit.SECONDS)) {
                    break;
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Waits for the game to start in the game session.
     */
    public void waitForGame() {
        try {
            while (this.gameSessionStatus.equals(GameSessionStatus.NEW)) {
                if (latch.await(1, TimeUnit.SECONDS)) {
                    break;
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Terminates the game in the session.
     */
    public synchronized void terminateGame() {
        this.setGameSessionStatus(GameSessionStatus.FINISHED);
        this.latch.countDown();
    }
}
