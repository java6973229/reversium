/**
 * Represents a response for creating a game.
 * Contains the ID of the created game.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gs.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateGameResponse {
    private Long gameId; // The ID of the created game
}
