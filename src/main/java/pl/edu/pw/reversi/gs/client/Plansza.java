package pl.edu.pw.reversi.gs.client;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Plansza {
    private int player;
    private int row;
    private int col;
    private GameStatus gameStatus;
}
