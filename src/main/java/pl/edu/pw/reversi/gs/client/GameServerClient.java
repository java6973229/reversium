/**
 * This class represents a client for interacting with the game server for Reversi.
 * It provides methods for creating and managing game sessions, making moves, and retrieving game information.
 * The client uses RESTful API calls to communicate with the server.
 *
 * @Author Damian Pasik
 * @Version 0.9
 * @Since 11.06.2023
 */
package pl.edu.pw.reversi.gs.client;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.edu.pw.reversi.gs.model.Board;

@Component
public class GameServerClient implements IGameServer {

    private final RestTemplate restTemplate;
    @Value("${gameServer.ip}")
    private String ip;
    @Value("${gameServer.port}")
    private Integer port;
    @Value("${gameServer.ssl}")
    private Boolean ssl;
    private String baseURL;

    /**
     * Constructs a new GameServerClient instance.
     * Initializes the RestTemplate used for making HTTP requests to the game server.
     */
    public GameServerClient() {

        this.restTemplate = new RestTemplate();
    }

    /**
     * Initializes the base URL of the game server after the client has been constructed.
     * The base URL is constructed based on the server's IP address, port number, and SSL configuration.
     */
    @PostConstruct
    public void init() {
        this.baseURL = (Boolean.TRUE.equals(ssl) ? "https://" : "http://") + ip + ":" + port;
    }

    /**
     * Sends a POST request to the game server to create a new game session.
     *
     * @return The unique identifier (gameId) for the created game session.
     */
    public Long createGame() {
        try {
            ResponseEntity<Long> response = restTemplate.postForEntity(baseURL + "/create", null, Long.class);
            return response.getBody();
        } catch (Exception e) {
            return handleException(e);
        }
    }

    /**
     * Sends a DELETE request to the game server to remove the specified game session.
     *
     * @param gameId The unique identifier of the game session to be removed.
     * @return True if the game was successfully removed, otherwise false.
     */
    public Boolean removeGame(Long gameId) {
        try {
            ResponseEntity<Boolean> response = restTemplate.exchange(baseURL + "/remove/" + gameId,
                    HttpMethod.DELETE, null, Boolean.class);
            return response.getBody();
        } catch (Exception e) {
            return handleException(e);
        }
    }

    /**
     * Sends a GET request to the game server to make a move in the specified game session.
     *
     * @param gameId   The unique identifier of the game session.
     * @param playerId The unique identifier of the player making the move.
     * @param x        The x-coordinate of the move on the game board.
     * @param y        The y-coordinate of the move on the game board.
     * @return The updated Plansza instance representing the state of the game after the move.
     */
    public Plansza makeMove(Long gameId, Long playerId, Integer x, Integer y) {
        try {
            ResponseEntity<Plansza> response = restTemplate.getForEntity(baseURL + "/plansza/" + gameId
                    + "?player=" + playerId + "&col=" + x + "&row=" + y, Plansza.class);
            return response.getBody();
        } catch (Exception e) {
            return handleException(e);
        }
    }

    /**
     * Sends a GET request to the game server to retrieve information about the specified game session.
     *
     * @param gameId The unique identifier of the game session.
     * @return The Plansza instance representing the current state of the game.
     */
    public Plansza gameInfo(Long gameId) {
        try {
            ResponseEntity<Plansza> response = restTemplate.getForEntity(baseURL + "/plansza/" + gameId, Plansza.class);
            return response.getBody();
        } catch (Exception e) {
            return handleException(e);
        }
    }

    /**
     * Handles an exception by printing the stack trace and returning null.
     *
     * @param e The exception to handle.
     * @return null
     */
    private <T> T handleException(Exception e) {
        e.printStackTrace();
        return null;
    }
}
