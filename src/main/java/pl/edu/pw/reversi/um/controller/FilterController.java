package pl.edu.pw.reversi.um.controller;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * A custom filter for pre-processing HTTP requests.
 * <p>
 * This filter is intended for future authentication of client UI servers.
 * OncePerRequestFilter ensures that this filter is only executed once per request.
 * Currently, it's a stub that simply passes the request and response without modifying them.
 * Future implementations should include logic for authentication and other pre-processing tasks as required.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Component
public class FilterController extends OncePerRequestFilter {

    /**
     * Filters HTTP requests. Currently, this method does not modify the request or response.
     *
     * @param request      the HttpServletRequest
     * @param response     the HttpServletResponse
     * @param filterChain  the FilterChain
     * @throws ServletException in case of filter processing errors
     * @throws IOException      in case of I/O errors
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        filterChain.doFilter(request, response);
    }
}
