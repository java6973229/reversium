package pl.edu.pw.reversi.um.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pw.reversi.um.entity.Users;

import java.util.List;
import java.util.Optional;

/**
 * UsersRepository is an interface that extends CrudRepository for basic CRUD operations.
 * It's a part of the data access layer and provides methods for retrieving Users based on certain conditions.
 *
 * <p>
 * This repository interface includes methods for:
 * <ul>
 *   <li>Finding a user by email</li>
 *   <li>Retrieving all users ordered by total points in descending order</li>
 * </ul>
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    /**
     * Retrieves a user by email address.
     *
     * @param email the email address to search for
     * @return an Optional containing the User if found, or an empty Optional if not found
     */
    Optional<Users> findByEmail(String email);

    /**
     * Retrieves all users from the repository and sorts them in descending order based on total points.
     *
     * @return a List of Users ordered by total points in descending order
     */
    List<Users> findAllByOrderByTotalPointsDesc();
}
