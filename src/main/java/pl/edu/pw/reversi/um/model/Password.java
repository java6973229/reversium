package pl.edu.pw.reversi.um.model;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Utility class Password provides methods for hashing and comparing passwords.
 * This class is designed to be used for securing passwords before storage and
 * for verifying passwords during authentication.
 *
 * <p>
 * The class makes use of the SHA-256 algorithm for hashing passwords and Base64
 * encoding to encode the hashed byte array as a String.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
public class Password {

    /**
     * Private constructor to prevent instantiation of this utility class.
     */
    private Password() {
    }

    /**
     * Hashes the input password using the SHA-256 algorithm and encodes it
     * as a Base64 string.
     *
     * @param password the plain text password to hash
     * @return a Base64 encoded string representing the hashed password, or null if the hashing algorithm is not available
     */
    public static String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(hashBytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Compares a plain text password to a hashed password to determine if they match.
     *
     * <p>
     * This is typically used for verifying passwords during user authentication.
     * </p>
     *
     * @param plainPassword        the plain text password to compare
     * @param storedHashedPassword the hashed password to compare it against
     * @return true if the plain text password matches the hashed password, false otherwise or if the hashing algorithm is not available
     */
    public static boolean comparePasswords(String plainPassword, String storedHashedPassword) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashBytes = md.digest(plainPassword.getBytes(StandardCharsets.UTF_8));
            String base64Hash = Base64.getEncoder().encodeToString(hashBytes);
            return base64Hash.equals(storedHashedPassword);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
    }
}
