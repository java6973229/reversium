package pl.edu.pw.reversi.um.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pw.reversi.um.entity.Users;
import pl.edu.pw.reversi.um.repository.UsersRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The UsersManagementService is a spring-managed service class that provides the core business
 * logic for managing users. It interacts with the UsersRepository for database interactions
 * and maintains an in-memory list of authenticated users.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Service
public class UsersManagementService implements IUsersManagementService {
    private final UsersRepository usersRepository;
    private final List<Users> authenticatedUsers = Collections.synchronizedList(new ArrayList<>());

    /**
     * Constructs a UsersManagementService instance and initializes the authenticatedUsers list.
     *
     * @param usersRepository The users repository for database interactions.
     */
    @Autowired
    public UsersManagementService(UsersRepository usersRepository) {

        this.usersRepository = usersRepository;
    }

    /**
     * Authenticates the given user by verifying its credentials against the stored records
     * in the database. If authenticated, the user is added to the list of authenticated users.
     *
     * @param user The user entity containing credentials to be authenticated.
     * @return {@code Boolean.TRUE} if the user is successfully authenticated, {@code Boolean.FALSE} otherwise.
     */
    public Boolean authenticateUser(Users user) {
        try {
            Optional<Users> authenticatedUser = authenticatedUsers.stream()
                    .filter(u -> u.getEmail().equals(user.getEmail())).findAny();
            if (!authenticatedUser.isPresent()) {
                Optional<Users> existingUser = usersRepository.findByEmail(user.getEmail());
                if (existingUser.isPresent() && user.getHashedPassword()
                        .equals(existingUser.get().getHashedPassword())) {
                        user.setUserId(existingUser.get().getUserId());
                        authenticatedUsers.add(user);
                        return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /**
     * Logs out the user associated with the provided user ID. This is achieved by
     * removing the user from the list of authenticated users.
     *
     * @param userId The ID of the user to be logged out.
     * @return {@code Boolean.TRUE} if the user is successfully logged out, {@code Boolean.FALSE} otherwise.
     */
    public Boolean logoutUser(Long userId) {
        try {
            Optional<Users> authenticatedUser = authenticatedUsers.stream()
                    .filter(user -> user.getUserId().equals(userId))
                    .findAny();
            if (authenticatedUser.isPresent()) {
                authenticatedUsers.remove(authenticatedUser.get());
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /**
     * Retrieves a list of all users in descending order by total points.
     * It fetches this data from the users repository.
     *
     * @return A list of users sorted by total points in descending order.
     */
    public List<Users> getAllUsers() {
        List<Users> users = new ArrayList<>();
        try {
            users.addAll(usersRepository.findAllByOrderByTotalPointsDesc());
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return users;
        }
    }

    /**
     * Retrieves the user corresponding to the given email address.
     *
     * @param email The email address of the desired user.
     * @return The User entity if found, or {@code null} if not found.
     */
    public Users getUserByEmail(String email) {
        try {
            Optional<Users> existingUser = usersRepository.findByEmail(email);
            return existingUser.orElse(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves the user corresponding to the given user ID.
     *
     * @param userId The ID of the desired user.
     * @return The User entity if found, or {@code null} if not found.
     */
    public Users getUserById(Long userId) {
        try {
            Optional<Users> existingUser = usersRepository.findById(userId);
            return existingUser.orElse(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Registers a new user and saves it in the repository. The method sets the total points
     * to zero if not provided, and records the current timestamp as the registration and modification date.
     *
     * @param user The user entity to be registered.
     * @return {@code Boolean.TRUE} if registration is successful, {@code Boolean.FALSE} otherwise.
     */
    public Boolean registerUser(Users user) {
        try {
            if(user.getTotalPoints() == null) {
                user.setTotalPoints(0L);
            }
            user.setRegistrationDate(LocalDateTime.now());
            user.setModificationDate(LocalDateTime.now());
            usersRepository.save(user);
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /**
     * Saves the given user in the repository. This method can be used for creating new users
     * or updating existing ones.
     *
     * @param user The user entity to be saved.
     */
    public void saveUser(Users user) {
        try {
            usersRepository.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Unregisters a user by deleting their record from the repository based on the provided user ID.
     *
     * @param userId The ID of the user to be unregistered.
     * @return {@code Boolean.TRUE} if unregistration is successful, {@code Boolean.FALSE} otherwise.
     */
    public Boolean unregisterUser(Long userId) {
        try {
            usersRepository.deleteById(userId);
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /**
     * Updates an existing user's information in the repository. The method is synchronized to avoid
     * concurrent modifications. It updates the name and hashed password fields of the user.
     *
     * @param user The user entity containing the updated information.
     * @return {@code Boolean.TRUE} if update is successful, {@code Boolean.FALSE} otherwise.
     */
    public synchronized Boolean updateUser(Users user) {
        try {
            Optional<Users> existingUser = usersRepository.findById(user.getUserId());
            if (existingUser.isPresent()) {
                existingUser.get().setName(user.getName());
                String hashedPassword = user.getHashedPassword();
                if (hashedPassword != null && !hashedPassword.isEmpty()) {
                    existingUser.get().setHashedPassword(hashedPassword);
                }
                usersRepository.save(existingUser.get());
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }
}
