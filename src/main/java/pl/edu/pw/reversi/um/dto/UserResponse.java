package pl.edu.pw.reversi.um.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * The UserResponse class represents the data that is sent as a response
 * after processing a user request. It contains the user's ID, email, name,
 * and total points which might represent some form of score or reputation.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Getter
@Setter
public class UserResponse {

    /**
     * The unique identifier of the user. This is typically used for
     * distinguishing between different users and for data retrieval.
     */
    private Long userId;

    /**
     * The email address associated with the user account. This may also be
     * used as a username for authentication.
     */
    private String email;

    /**
     * The user's name. This could be the full name of the user and is used
     * for identification purposes within the application.
     */
    private String name;

    /**
     * The total points associated with the user. This could represent a score,
     * reputation, or any other form of accumulated points relevant to the application.
     */
    private Long totalPoints;
}
