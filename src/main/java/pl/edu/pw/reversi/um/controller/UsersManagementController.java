package pl.edu.pw.reversi.um.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.pw.reversi.um.dto.AuthenticateUserRequest;
import pl.edu.pw.reversi.um.dto.UserRequest;
import pl.edu.pw.reversi.um.dto.UserResponse;
import pl.edu.pw.reversi.um.entity.Users;
import pl.edu.pw.reversi.um.service.IUsersManagementService;

import java.util.List;

/**
 * A REST controller to manage user related operations such as authentication,
 * registration, retrieval, and de-registration of users.
 * <p>
 * This controller provides a RESTful API to perform CRUD operations on users.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@RestController
@RequestMapping("/api/user")
public class UsersManagementController {

    private final IUsersManagementService usersManagementService;

    private final ModelMapper modelMapper;

    /**
     * Initializes a new instance of the {@link UsersManagementController} class.
     *
     * @param usersManagementService The user management service.
     * @param modelMapper            The model mapper.
     */
    @Autowired
    public UsersManagementController(IUsersManagementService usersManagementService,
                                     ModelMapper modelMapper) {
        this.usersManagementService = usersManagementService;
        this.modelMapper = modelMapper;
    }

    /**
     * Authenticates a user.
     *
     * @param request The authentication request containing the user's credentials.
     * @return A {@link ResponseEntity} containing a boolean indicating the success of the authentication.
     */
    @PostMapping("/authenticate")
    public ResponseEntity<Boolean> authenticate(@RequestBody AuthenticateUserRequest request) {
        try {
            Users user = modelMapper.map(request, Users.class);
            return ResponseEntity.ok(usersManagementService.authenticateUser(user));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Logs out a user.
     *
     * @param userId The ID of the user to log out.
     * @return A {@link ResponseEntity} containing a boolean indicating the success of the logout.
     */
    @DeleteMapping("/logout/{userId}")
    public ResponseEntity<Boolean> logout(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(usersManagementService.logoutUser(userId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Retrieves a list of all users.
     *
     * @return A {@link ResponseEntity} containing a list of {@link UserResponse}.
     */
    @GetMapping("/list")
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        try {
            List<Users> users = usersManagementService.getAllUsers();
            List<UserResponse> response = users.stream()
                    .map(user -> modelMapper.map(user, UserResponse.class))
                    .toList();
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Retrieves a user by email address.
     *
     * @param email The email address of the user to retrieve.
     * @return A {@link ResponseEntity} containing the {@link UserResponse}.
     */
    @GetMapping("/get/{email}")
    public ResponseEntity<UserResponse> getUserByEmail(@PathVariable String email) {
        try {
            Users user = usersManagementService.getUserByEmail(email);
            UserResponse response = modelMapper.map(user, UserResponse.class);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Registers a new user.
     *
     * @param request The registration request containing the user's information.
     * @return A {@link ResponseEntity} containing a boolean indicating the success of the registration.
     */
    @PostMapping("/register")
    public ResponseEntity<Boolean> register(@RequestBody UserRequest request) {
        try {
            Users user = modelMapper.map(request, Users.class);
            return ResponseEntity.ok(usersManagementService.registerUser(user));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Unregisters a user.
     *
     * @param userId The ID of the user to unregister.
     * @return A {@link ResponseEntity} containing a boolean indicating the success of the unregistration.
     */
    @DeleteMapping("/unregister/{userId}")
    public ResponseEntity<Boolean> unregister(@PathVariable Long userId) {
        try {
            return ResponseEntity.ok(usersManagementService.unregisterUser(userId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    /**
     * Updates a user's information.
     *
     * @param request The update request containing the user's updated information.
     * @return A {@link ResponseEntity} containing a boolean indicating the success of the update.
     */
    @PutMapping("/update")
    public ResponseEntity<Boolean> update(@RequestBody UserRequest request) {
        try {
            Users user = modelMapper.map(request, Users.class);
            return ResponseEntity.ok(usersManagementService.updateUser(user));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }
}
