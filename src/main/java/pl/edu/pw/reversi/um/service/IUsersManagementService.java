package pl.edu.pw.reversi.um.service;

import pl.edu.pw.reversi.um.entity.Users;

import java.util.List;

/**
 * The IUsersManagementService interface defines the contract for user management operations.
 * It outlines the essential functionalities for authenticating, registering, updating, and
 * retrieving user information. Implementing classes should provide the concrete implementations
 * of these operations.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
public interface IUsersManagementService {

    Boolean authenticateUser(Users user);
    Boolean logoutUser(Long userId);

    List<Users> getAllUsers();

    Users getUserByEmail(String email);

    Users getUserById(Long userId);

    Boolean registerUser(Users user);

    void saveUser(Users user);

    Boolean unregisterUser(Long userId);

    Boolean updateUser(Users user);
}