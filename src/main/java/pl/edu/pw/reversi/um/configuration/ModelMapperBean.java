package pl.edu.pw.reversi.um.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class that defines a singleton bean for ModelMapper.
 * <p>
 * ModelMapper is used to map objects from one type to another, which is useful
 * when converting entities to DTOs and vice versa.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Configuration
public class ModelMapperBean {

    /**
     * Defines a singleton bean for ModelMapper.
     *
     * @return A new instance of ModelMapper.
     */
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}