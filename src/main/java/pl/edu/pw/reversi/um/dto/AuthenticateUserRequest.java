package pl.edu.pw.reversi.um.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * The AuthenticateUserRequest class represents the data necessary for user authentication.
 * It contains the user's email and the hashed version of the user's password.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Getter
@Setter
public class AuthenticateUserRequest {

    /**
     * The email address associated with a user account.
     * This is used as a unique identifier for authentication.
     */
    private String email;

    /**
     * The hashed password associated with a user account.
     * Hashing the password provides an extra layer of security.
     */
    private String hashedPassword;
}
