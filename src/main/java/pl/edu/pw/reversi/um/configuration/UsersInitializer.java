package pl.edu.pw.reversi.um.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.edu.pw.reversi.um.entity.Users;
import pl.edu.pw.reversi.um.model.Password;
import pl.edu.pw.reversi.um.service.IUsersManagementService;

import java.time.LocalDateTime;

/**
 * Component for initializing user data in the database upon application startup.
 * <p>
 * This component is useful for populating the database with sample users for testing
 * and development purposes.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Component
public class UsersInitializer implements CommandLineRunner {

    private static final String PASSWORD = "2Sec4You";
    private final IUsersManagementService usersManagementService;

    /**
     * Constructs a new UsersInitializer with the provided user management service.
     *
     * @param usersManagementService the user management service to use for creating users.
     */
    @Autowired
    public UsersInitializer(IUsersManagementService usersManagementService) {
        this.usersManagementService = usersManagementService;
    }

    /**
     * Initializes the database with sample user data upon application startup.
     *
     * @param args command line arguments.
     */
    @Override
    public void run(String... args) {
        createUser("Neil Armstrong", "neil.armstrong@example.com", 100L);
        createUser("Yuri Gagarin", "yuri.gagarin@example.com", 200L);
        createUser("Sally Ride", "sally.ride@example.com", 400L);
        createUser("Chris Hadfield", "chris.hadfield@example.com", 300L);
    }

    /**
     * Creates and registers a new user with the given name, email and points if not already exist.
     *
     * @param name   the name of the user.
     * @param email  the email of the user.
     * @param points the total points of the user.
     */
    private void createUser(String name, String email, Long points) {
        if (usersManagementService.getUserByEmail(email) == null) {
            Users user = new Users();
            user.setName(name);
            user.setEmail(email);
            user.setHashedPassword(Password.hashPassword(PASSWORD));
            user.setTotalPoints(points);
            user.setRegistrationDate(LocalDateTime.now());
            user.setModificationDate(LocalDateTime.now());
            usersManagementService.registerUser(user);
        }
    }
}
