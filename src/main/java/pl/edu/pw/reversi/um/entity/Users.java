package pl.edu.pw.reversi.um.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Represents a User entity within the application.
 * <p>
 * This entity stores information related to a user, including their email, name,
 * hashed password, total points, registration date, and modification date.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Entity
@Getter
@Setter
public class Users {

    /**
     * The unique identifier for the user. This is automatically generated.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;

    /**
     * The user's email address. This must not be null or blank and must be unique.
     */
    @NotBlank
    @Column(unique=true)
    private String email;

    /**
     * The user's name. This must not be null or blank.
     */
    @NotBlank
    private String name;

    /**
     * The user's password stored in hashed format. This must not be null or blank.
     */
    @NotBlank
    private String hashedPassword;

    /**
     * The total points accumulated by the user. This must not be null.
     */
    @NotNull
    private Long totalPoints;

    /**
     * The date and time when the user was registered. This must not be null.
     */
    @NotNull
    private LocalDateTime registrationDate;

    /**
     * The date and time when the user's information was last modified. This must not be null.
     */
    @NotNull
    private LocalDateTime modificationDate;
}
