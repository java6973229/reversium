package pl.edu.pw.reversi.um.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * The UserRequest class represents the data associated with a user.
 * This class is typically used for creating or updating user information.
 * It contains the user's ID, email, name, and hashed password.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@Getter
@Setter
public class UserRequest {

    /**
     * The unique identifier of the user. This is generally used for
     * distinguishing between different users and for data retrieval.
     */
    private Long userId;

    /**
     * The email address associated with the user account. This may also be
     * used as a username for authentication.
     */
    private String email;

    /**
     * The user's name. This could be the full name of the user and is used
     * for identification purposes within the application.
     */
    private String name;

    /**
     * The hashed password associated with a user account.
     * Storing the hashed version of the password provides an additional
     * layer of security compared to storing plaintext passwords.
     */
    private String hashedPassword;
}
