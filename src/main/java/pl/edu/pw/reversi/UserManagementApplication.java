package pl.edu.pw.reversi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * UserManagementApplication is the main entry point for the User Management application.
 * It initializes the Spring Boot application with configurations for scheduling, entity scanning,
 * and JPA repositories. Additionally, this class is responsible for reading and configuring application-specific
 * properties such as server port, remote IP, remote port, and SSL preferences.
 *
 * <p>
 * This class sets up the Spring Boot environment by reading in the application properties and passing
 * them as system properties.
 * </p>
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
@SpringBootApplication
@EnableScheduling
@EntityScan(basePackages = "pl.edu.pw.reversi")
@EnableJpaRepositories(basePackages = "pl.edu.pw.reversi")
public class UserManagementApplication {

    /**
     * The main method is the entry point for the Spring Boot application.
     *
     * <p>
     * This method initializes the Spring application context, sets up the listeners for the ApplicationEnvironmentPreparedEvent,
     * and starts the application. It also reads configuration values from the application's properties and sets them as
     * system properties.
     * </p>
     *
     * @param args command-line arguments passed to the application
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(UserManagementApplication.class);
        app.addListeners((ApplicationListener<ApplicationEnvironmentPreparedEvent>) event -> {
            Environment env = event.getEnvironment();
            System.setProperty("server.port", env.getProperty("localPort", "8080"));
            System.setProperty("server.ssl.enabled", env.getProperty("localSSL", "false"));
            System.setProperty("gameServer.ip", env.getProperty("remoteIp", "127.0.0.1"));
            System.setProperty("gameServer.port", env.getProperty("remotePort", "8090"));
            System.setProperty("gameServer.ssl", env.getProperty("remoteSSL", "false"));
        });
        app.run(args);
    }
}